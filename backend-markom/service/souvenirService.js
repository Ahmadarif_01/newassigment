module.exports = exports = (server, pool) => {
    server.post('/api/get_souvenir', (req, res) => {
        const { searchCode, searchName, searchCB, searchDT, searchUnit, order, page, pagesize } = req.body;
        let qFilterCode = searchCode != "" ? ` AND s."code" LIKE '%${searchCode}%' ` : ``;
        let qFilterName = searchName != "" ? ` AND s."name" LIKE '%${searchName}%' ` : ``;
        let qFilterCB = searchCB != "" ? ` AND s."created_by" LIKE '%${searchCB}%' ` : ``;
        let qFilterDT = searchDT != "" ? ` AND s."created_date"::text LIKE '%${searchDT}%' ` : ``;
        let qFilterUnit = searchUnit != "" ? ` AND u."name" LIKE '%${searchUnit}%' ` : ``;
        let qOrder = order != "" ? `ORDER BY s."code" DESC` : `ORDER BY s."code"`
        let perpageName = (page - 1) * pagesize;

        var query = `Select s."name", s."code", s."created_by", TO_CHAR(s."created_date" :: DATE, 'yyyy-mm-dd') as tgl , u."id" as "unitId", u."name" as "unitName"
        from "souvenir" as s
        join "unit" as u on s.m_unit_id = u.id where s."is_delete" = false
        ${qFilterCode} ${qFilterName} ${qFilterCB} ${qFilterDT} ${qFilterUnit} ${qOrder} LIMIT ${pagesize} 
        OFFSET ${perpageName}` 

        // console.log(query);
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/getAll_souvenir', (req, res) => {

        var query = `Select s."id", s."name", s."code", s."created_by", TO_CHAR(s."created_date" :: DATE, 'yyyy-mm-dd') as tgl , u."id" as "unitId", u."name" as "unitName"
        from "souvenir" as s
        join "unit" as u on s.m_unit_id = u.id where s."is_delete" = false`

        // console.log(query);
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });



    server.get('/api/get_souvenirCode/:code', (req, res) => {
        const code = req.params.code
        var query = `Select s.*, u."id" as "unitId", u."name" as "unitName"
        from "souvenir" as s
        join "unit" as u on s.m_unit_id = u.id where s."code" = '${code}'`

        // console.log(query);
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    function getSouvenirCode(callback) {
        var query = `Select "code" From "souvenir"
        ORDER BY "code" DESC LIMIT 1`;
        var newCode = `SV-`;
        pool.query(query, (error, result) => {
            if (error) {
                return {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                }
            } else {
                if (result.rows.length > 0) {
                    var codeArr = result.rows[0].code.split('-');
                    newCode = newCode + ("0000" + (parseInt(codeArr[1]) + 1).toString()).slice(-4);
                    console.log(codeArr[1])
                    return callback(newCode);
                } else {
                    newCode = newCode + "0001";
                    return callback(newCode);
                }

            }

        });
    }

    server.post('/api/souvenir_post', (req, res) => {
        console.log(req.body);

        const {
            name,
            description,
            mUnitId,
        } = req.body;

        let desc = description != "" ? `'${description}'` : null;

        getSouvenirCode(code => {
            var query = `INSERT INTO "souvenir"(
               "created_by", "created_date", "code", "name", "description", "is_delete", "m_unit_id")
               VALUES('Administator', current_timestamp, '${code}', '${name}', ${desc}, false, '${mUnitId}');`

            console.log(query)

            pool.query(query, (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(201, {
                        success: true,
                        result: "Berhasil Disimpan"
                    })
                }
            });
        })
    })

    server.put('/api/updateSouvenir/:code', (req, res) => {
        const code = req.params.code;

        const {
            name,
            description,
            mUnitId,
        } = req.body;

        let desc = description != "" ? `'${description}'` : null;

        var query = `UPDATE "souvenir" SET "updated_by" = 'Administator', "updated_date" = current_timestamp,
        "name" = '${name}', "description" = ${desc}, "m_unit_id" = ${mUnitId} WHERE "code" = '${code}';`

        // console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Update Data"
                })
            }
        });
    })

    server.put('/api/deleteSouvenir/:code', (req, res) => {
        const code = req.params.code;

        var query = `UPDATE "souvenir" SET "updated_by" = 'administator', "updated_date" = current_timestamp,
        "is_delete" = true WHERE "code" = '${code}';`

        // console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Hapus Data"
                })
            }
        });
    })

    server.post('/api/cd_souvenir', (req, res) => {
        const { searchCode, searchName, searchCB, searchDT, searchUnit, order, page, pagesize } = req.body;
        let qFilterCode = searchCode != "" ? ` AND s."code" LIKE '%${searchCode}%' ` : ``;
        let qFilterName = searchName != "" ? ` AND s."name" LIKE '%${searchName}%' ` : ``;
        let qFilterCB = searchCB != "" ? ` AND s."created_by" LIKE '%${searchCB}%' ` : ``;
        let qFilterDT = searchDT != "" ? ` AND s."created_date" LIKE '%${searchDT}%' ` : ``;
        let qFilterUnit = searchUnit != "" ? ` AND a."name" LIKE '%${searchUnit}%' ` : ``;

        var query = `select count("id") as totaldata from "souvenir" where "is_delete" = false  ${qFilterCode}
        ${qFilterName} ${qFilterCB} ${qFilterDT} ${qFilterUnit}`;
        // console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })

    });
}