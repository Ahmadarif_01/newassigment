module.exports = exports = (server, pool) => {
  function getCodeEvent(callback) {
    var date = new Date();
    var dd = date.getDate();
    var mm = "0" + (date.getMonth() + 1);
    var yyyy = date.getFullYear().toString();
    var yy = yyyy.slice(2, 4);
    var query = `select "code" from "t_event" ORDER BY "code" DESC LIMIT 1`;
    var newCode = `TRWOEV${dd}${mm}${yy}-`;

    pool.query(query, (error, result) => {
      if (error) {
        return {
          success: false,
          result: {
            detail: error.detail,
          },
        };
      } else {
        if (result.rows.length > 0) {
          var codeArr = result.rows[0].code.split("-");
          newCode =
            newCode +
            ("00000" + (parseInt(codeArr[1]) + 1).toString()).slice(-5);
          console.log(codeArr[1]);
          return callback(newCode);
        } else {
          newCode = newCode + "00001";
          return callback(newCode);
        }
      }
    });
  }

  server.post("/api/t_event_post", (req, res) => {
    const { eventname, startdate, enddate, place, budget, note } = req.body;

    let n = note == "" ? null : `'${note}'`;

    getCodeEvent((code) => {
      var query = `INSERT INTO "t_event"(
            "code", "event_name", "start_date", "end_date", "place",
            "budget", "request_by", "request_date", "note", "status",
            "created_by", "created_date") VALUES ('${code}', '${eventname}', '${startdate}',
            '${enddate}', '${place}', '${budget}', '1', current_timestamp,
            ${n}, '1', 'Sahid Triambudhi', current_timestamp )`;

      // console.log(query);
      pool.query(query, (error, result) => {
        if (error) {
          res.send(400, {
            success: false,
            result: error,
          });
        } else {
          res.send(201, {
            success: true,
            result: "Berhasil Disimpan",
          });
        }
      });
    });
  });

  server.put("/api/t_event_update/:id", (req, res) => {
    const id = req.params.id;
    const {
      eventname,
      startdate,
      enddate,
      place,
      budget,
      note,
      assign_to,
    } = req.body;

    let n = note == "" ? null : `'${note}'`;

    if (assign_to == null) {
      var query = `UPDATE "t_event" SET "event_name" = '${eventname}',
      "start_date" = '${startdate}', "end_date" = '${enddate}',
      "place" = '${place}', "budget" = '${budget}', "note" = ${n},
      "updated_by" = 'Sahid Triambudhi', "updated_date" = current_timestamp where "id" = ${id}`;
    } else {
      var query = `UPDATE "t_event" SET
      "updated_by" = 'Sahid Triambudhi', "updated_date" = current_timestamp,
      "assign_to" = ${assign_to}, "status" = '2' where "id" = ${id}`;
    }

    console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Update Data",
        });
      }
    });
  });


  server.put("/api/t_event_rejected/:id", (req, res) => {
    const id = req.params.id;
    const {
     rejectreason,
    } = req.body;

    let n = rejectreason == "" ? null : `'${rejectreason}'`;

      var query = `UPDATE "t_event" SET
      "updated_by" = 'Sahid Triambudhi', "updated_date" = current_timestamp,
      "status" = '0', "reject_reason" = ${n} where "id" = ${id}`;

    console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Data Reject",
        });
      }
    });
  });

  server.put("/api/t_event_done/:id", (req, res) => {
    const id = req.params.id;

      var query = `UPDATE "t_event" SET
      "updated_by" = 'Sahid Triambudhi', "updated_date" = current_timestamp,
      "status" = '3' where "id" = ${id}`;

    // console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Data Closed",
        });
      }
    });
  });

  server.post("/api/get_t_event", (req, res) => {
    const {
      searchCode,
      searchRB,
      searchRD,
      searchST,
      searchCD,
      searchCB,
      order,
      page,
      pagesize,
    } = req.body;

    let qFilterCode =
      searchCode !== "" ? `AND e."code" LIKE '%${searchCode}%'` : ``;
    let qFilterRB =
      searchRB !== "" ? `AND e."request_by" LIKE '%${searchRB}%'` : ``;
    let qFilterRD =
      searchRD !== "" ? `AND e."request_date"::text LIKE '%${searchRD}%'` : ``;
    let qFilterST =
      searchST !== "" ? `AND e."status" LIKE '%${searchST}%'` : ``;
    let qFilterCD =
      searchCD !== "" ? `AND e."created_date"::text LIKE '%${searchCD}%'` : ``;
    let qFilterCB =
      searchCB !== "" ? `AND e."created_by" LIKE '%${searchCB}%'` : ``;
    let qOrder = order != "" ? `ORDER BY e."code" DESC` : `ORDER BY e."code"`;
    let perpageName = (page - 1) * pagesize;

    var query = `select e."id", k."first_name", k."last_name", e."code", e."request_by", TO_CHAR(e."request_date" :: DATE, 'yyyy-mm-dd') as tgl, e."status", 
    TO_CHAR(e."created_date" :: DATE, 'yyyy-mm-dd') as tglcd,
	  e."created_by" from "t_event" as e
    join "employee" as k on k.id = e.request_by where e."status" != '0'
    ${qFilterCode} ${qFilterRB} ${qFilterRD} ${qFilterST} ${qFilterCD}
    ${qFilterCB} ${qOrder} LIMIT ${pagesize} OFFSET ${perpageName}`;

    // console.log(query)
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  server.get("/api/get_id_event/:id", (req, res) => {
    const id = req.params.id;

    var query = `select "id", "reject_reason" as rejectReason, "status", "assign_to", "code", "note", "event_name" as eventname, TO_CHAR("start_date" :: DATE, 'yyyy-mm-dd') as startdate, TO_CHAR("end_date" :: DATE, 'yyyy-mm-dd') as enddate, "place", "budget" from "t_event" where "id" = ${id}`;
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  server.get("/api/get_employee_assignto", (req, res) => {
    var query = `select e."id", e."first_name", e."last_name", r."name" from "m_role" as r
    join "m_user" as u on u.m_role_id = r.id
    join "employee" as e on e.id = u.m_employee_id WHERE r."name" = 'Staff'`;

    // console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });
};
