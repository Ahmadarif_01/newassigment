module.exports = exports = (server, pool) => {
  function getCodeSouvenir(callback) {
    var date = new Date();
    var dd = date.getDate();
    var mm = "0" + (date.getMonth() + 1);
    var yyyy = date.getFullYear().toString();
    var yy = yyyy.slice(2, 4);
    var query = `select "code" from "t_souvenir" ORDER BY "code" DESC LIMIT 1`;
    var newCode = `TRSV${dd}${mm}${yy}-`;

    pool.query(query, (error, result) => {
      if (error) {
        return {
          success: false,
          result: {
            detail: error.detail,
          },
        };
      } else {
        if (result.rows.length > 0) {
          var codeArr = result.rows[0].code.split("-");
          newCode =
            newCode +
            ("00000" + (parseInt(codeArr[1]) + 1).toString()).slice(-5);
          console.log(codeArr[1]);
          return callback(newCode);
        } else {
          newCode = newCode + "00001";
          return callback(newCode);
        }
      }
    });
  }

  server.post("/api/t_souvenir_post", (req, res) => {
    const { type, received_by, tgl, note } = req.body;

    let n = note == "" ?  null : `'${note}'`;

    getCodeSouvenir((code) => {
      var query = `INSERT INTO "t_souvenir"(
              "created_by", "created_date", "code", "received_by", "received_date",
              "note", "type") VALUES ('Administator', current_timestamp, '${code}', ${received_by}, '${tgl}', ${n}, '${type}')`;
      // console.log(query);

      pool.query(query, (error, result) => {
        if (error) {
          res.send(400, {
            success: false,
            result: error,
          });
        } else {
          res.send(201, {
            success: true,
            result: "Berhasil Disimpan",
          });
        }
      });
    });
  });

  server.put("/api/t_souvenir_update/:id", (req, res) => {
    const id = req.params.id;
    const { type, received_by, tgl, note } = req.body;

    let n = note == "" ?  null : `'${note}'`;

    var query = `UPDATE "t_souvenir" SET "update_by" = 'Administator',
    "updated_date" = current_timestamp, "type" = '${type}', "received_by" = ${received_by},
    "received_date" = '${tgl}', "note" = ${n} where "id" = ${id}`;

    // console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Update Data",
        });
      }
    });
  });

  server.post("/api/get_t_souvenir", (req, res) => {
    const {
      searchCode,
      searchRB,
      searchRD,
      searchCD,
      searchCB,
      order,
      page,
      pagesize,
    } = req.body;

    let qFilterCode =
      searchCode != "" ? `AND t."code" LIKE '%${searchCode}%'` : ``;
    let qFilterRB =
      searchRB != "" ? `AND t."received_by" LIKE '%${searchRB}%'` : ``;
    let qFilterRD =
      searchRD != "" ? `AND t."received_date"::text LIKE '%${searchRD}%'` : ``;
    let qFilterCD =
      searchCD != "" ? `AND t."created_date"::text LIKE '%${searchCD}%'` : ``;
    let qFilterCB =
      searchCB != "" ? `AND t."created_by" LIKE '%${searchCB}%'` : ``;
    let qOrder = order != "" ? `ORDER BY t."code" DESC` : `ORDER BY t."code"`;
    let perpageName = (page - 1) * pagesize;

    var query = `select t."id", t."code", e."first_name", e."last_name", TO_CHAR(t."received_date" :: DATE, 'yyyy-mm-dd') as tgl, t."created_by",
    TO_CHAR(t."created_date" :: DATE, 'yyyy-mm-dd') as tglcd  from "t_souvenir" as t
    join "employee" as e on e.id = t.received_by ${qFilterCode} ${qFilterRB} ${qFilterRD} ${qFilterCD}
    ${qFilterCB} ${qOrder} LIMIT ${pagesize} OFFSET ${perpageName} `;
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  server.get("/api/get_id/:id", (req, res) => {
    const id = req.params.id;

    var query = `select t."id", t."code", e."first_name", e."last_name", t."received_by", TO_CHAR(t."received_date" :: DATE, 'yyyy-mm-dd') as tgl, t."created_by",
      t."created_date", t."note", t."type" from "t_souvenir" as t
      join "employee" as e on e.id = t.received_by WHERE t."id" = ${id}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });


  server.get("/api/get_employee", (req, res) => {
    var query = `select * from employee`;
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

};
