module.exports = exports = (server, pool) => {
  server.post("/api/get_employee", (req, res) => {
    const {
      searchCode,
      searchName,
      searchCN,
      searchDT,
      searchCB,
      order,
      page,
      pagesize,
    } = req.body;
    let qFilterCode =
      searchCode != "" ? `AND e."empolyee_number" LIKE '%${searchCode}%' ` : ``;
    let qFilterName =
      searchName != "" ? `AND e."first_name" LIKE '%${searchName}%' ` : ``;
    let qFilterCN = searchCN != "" ? `AND c."name" LIKE '%${searchCN}%' ` : ``;
    let qFilterDT =
      searchDT != "" ? `AND e."created_date"::text LIKE '%${searchDT}%' ` : ``;
    let qFilterCB =
      searchCB != "" ? `AND e."created_by" LIKE '%${searchCB}%' ` : ``;
    let qOrder =
      order != ""
        ? `ORDER BY e."empolyee_number" DESC`
        : `ORDER BY e."empolyee_number"`;
    let perpageName = (page - 1) * pagesize;

    var query = `Select e."id", e."first_name", e."last_name", e."empolyee_number" as code, e."created_by", TO_CHAR(e."created_date" :: DATE, 'yyyy-mm-dd') as tgl,
      c."id" as "companyId", c."name" as "companyName"
      from "employee" as e
      join "company" as c on e.m_company_id = c.id where e."is_delete" = false 
      ${qFilterCode}  ${qFilterName}  ${qFilterCN}  ${qFilterDT}  ${qFilterCB}  ${qOrder}
      LIMIT ${pagesize} OFFSET  ${perpageName}`;

    // console.log(query)

    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  server.get("/api/get_empolyeeNumber/:id", (req, res) => {
    const id = req.params.id;
    var query = `Select e."id", e."first_name", e."last_name", e."empolyee_number" as code,e."empolyee_number", e."email"
        , c."id" as "companyId", c."name" as "companyName"
        from "employee" as e
        join "company" as c on e.m_company_id = c.id where e."id" = '${id}'`;

    //console.log(query)

    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  //   function getEmpolyeeNumber(callback) {
  //       var query = `Select "empolyee_number" from "employee"
  //       ORDER BY "empolyee_number DESC LIMIT 1`;
  //       var newNumber = `18.03.26.`;
  //       pool.query(query, (error, result) => {
  //         if (error) {
  //             return {
  //                 success: false,
  //                 result: {
  //                     detail: error.detail
  //                 }
  //             }
  //         } else {
  //             if (result.rows.length > 0) {
  //                 var codeArr = result.rows[0].code.split('.');
  //                 newNumber = newNumber + ("00" + (parseInt(codeArr[5]) + 1).toString()).slice(-2);
  //                 console.log(codeArr[5])
  //                 return callback(newNumber);
  //             } else {
  //                 newNumber = newNumber + "01";
  //                 return callback(newNumber);
  //             }

  //         }

  //     });
  //   }

  server.get("/api/get_company", (req, res) => {
    var query = `select * from company where is_delete = false`;
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  server.post("/api/employee_post", (req, res) => {
    const { code, first_name, last_name, email, companyId } = req.body;

    let ln = last_name == "" ? null : `'${last_name}'`;
    let mail = email == "" ? null : `'${email}'`;

    var query = `INSERT INTO public.employee(
      empolyee_number, first_name, last_name, m_company_id, email, is_delete, created_by, created_date)
      VALUES('${code}', '${first_name}', ${ln}, ${companyId}, ${mail}, false, 'Administator', current_timestamp)`;

    //console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Disimpan",
        });
      }
    });
  });

  server.put("/api/updateEmployee/:id", (req, res) => {
    const id = req.params.id;

    const { code, first_name, last_name, email, companyId } = req.body;
    let ln = last_name == "" ? null : `'${last_name}'`;
    let mail = email == "" ? null : `'${email}'`;

    var query = `UPDATE "employee" SET "updated_by" = 'Administator', "updated_date" = current_timestamp, "empolyee_number" = '${code}',
      "first_name" = '${first_name}', "last_name" = ${ln}, "email" = ${mail}, "m_company_id" = ${companyId} WHERE "id" = ${id}`;

    ////console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Update Data",
        });
      }
    });
  });

  server.put("/api/deleteEmployee/:id", (req, res) => {
    const id = req.params.id;

    var query = `UPDATE "employee" SET "updated_by" = 'Administator', "updated_date" = current_timestamp,
        "is_delete" = true WHERE "id" = '${id}'`;

    // //console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: {
            detail: error.detail,
          },
        });
      } else {
        res.send(201, {
          success: true,
          result: "Berhasil Hapus Data",
        });
      }
    });
  });

  server.post("/api/cd_employee", (req, res) => {
    const { searchCode, searchName, searchCN, searchDT, searchCB } = req.body;
    let qFilterCode =
      searchCode != "" ? `AND e."empolyee_number" LIKE '%${searchCode}%' ` : ``;
    let qFilterName =
      searchName != ""
        ? `AND e."first_name" AND e."last_name" LIKE '%${searchName}%' `
        : ``;
    let qFilterCN = searchCN != "" ? `AND c."id" LIKE '%${searchCN}%' ` : ``;
    let qFilterDT =
      searchDT != "" ? `AND e."created_date" LIKE '%${searchDT}%' ` : ``;
    let qFilterCB =
      searchCB != "" ? `AND e."created_by" LIKE '%${searchCB}%' ` : ``;

    var query = `SELECT count("id") as totaldata from "employee" where "is_delete" = false
      ${qFilterCode}  ${qFilterName}  ${qFilterCN}  ${qFilterDT}  ${qFilterCB}`;
  });

  server.post("/api/cekData", (req, res) => {
    const { code } = req.body;

    if (code == code) {
      var query = `select count(*) as hitung from "employee" where is_delete = 'false' and "empolyee_number" = '${code}'`;
    } else {
      var query = `select count(*) as hitung from "employee" where is_delete = 'false' and "empolyee_number" != '${code}'`;
    }

    console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });

  server.post("/api/cekData_ubah", (req, res) => {
    const {
      id,
      code,
      first_name,
      last_name,
      email,
      companyId,
      empolyee_number,
    } = req.body;

    if (code == empolyee_number) {
      var query = `select count(*) as hitung from "employee" where is_delete = 'false' and "empolyee_number" != '${code}'
      and "first_name" = '${first_name}' and "last_name" = '${last_name}' and "email" = '${email}' and
      "m_company_id" = ${companyId}`;
    } else if (code != empolyee_number) {
      var query = `select count(*) as hitung from "employee" where is_delete = 'false' and "empolyee_number" = '${code}'`;
    } else {
      var query = `select count(*) as hitung from "employee" where is_delete = 'false' and "empolyee_number" = '${code}'
      and "first_name" = '${first_name}' and "last_name" = '${last_name}' and "email" = '${email}' and
      "m_company_id" = ${companyId}`;
    }

    console.log(query);
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });
};
