import React from "react";
import Table from "react-bootstrap/Table";
import FormInput from "./formInput";
import Approved from "./approved";
import Reject from "./reject";
import Detail from "./detail";
import Modal from "react-bootstrap/Modal";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import eventService from "../Service/eventService";

class Event extends React.Component {
  m_event = {
    eventname: "",
    startdate: "",
    enddate: "",
    place: "",
    budget: "",
    note: "",
    assign_to: "",
    rejectreason: "",
  };

  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_detail: false,
      open_approved: false,
      open_reject: false,
      m_event: this.m_event,
      list_event: [],
      list_employee: [],
      mode: "",
      errors: "",
      filter: {
        searchCode: "",
        searchRB: "",
        searchRD: "",
        searchST: "",
        searchCD: "",
        searchCB: "",
        order: "",
        page: "1",
        pagesize: "10",
      },
    };
  }

  changeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      m_event: {
        ...this.state.m_event,
        [name]: value,
      },
    });
  };

  select_employee = (name) => ({ target: { value } }) => {
    this.setState({
      m_event: {
        ...this.state.m_event,
        [name]: value,
      },
    });
  };

  open_CE = () => {
    this.setState({
      open_modal: true,
      errors: {},
      m_event: {
        eventname: "",
        startdate: "",
        enddate: "",
        place: "",
        budget: "",
        note: "",
      },
      mode: "create",
    });
  };

  cancel_CE = () => {
    this.setState({
      open_modal: false,
    });
  };

  cancel_approved = () => {
    this.setState({
      open_approved: false,
    });
  };

  closeDetail = () => {
    this.setState({
      open_detail: false,
    });
  };

  openReject = () => {
    this.setState({
      open_reject: true,
      open_approved: false,
    });
  };

  close_reject = () => {
    this.setState({
      open_reject: false,
      open_approved: true,
    });
  };

  openDetail = async (id) => {
    this.getEmployee();
    const respon = await eventService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_detail: true,
        m_event: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
  };

  handleValidation = () => {
    let fields = this.state.m_event;
    let errors = {};
    let formIsValid = true;

    if (!fields["eventname"]) {
      formIsValid = false;
      errors["eventname"] = "Jangan Kosong !";
    }

    if (!fields["startdate"]) {
      formIsValid = false;
      errors["startdate"] = "Jangan Kosong !";
    }

    if (!fields["enddate"]) {
      formIsValid = false;
      errors["enddate"] = "Jangan Kosong !";
    }

    if (!fields["place"]) {
      formIsValid = false;
      errors["place"] = "Jangan Kosong !";
    }

    if (!fields["budget"].match(/^[0-9,.]+$/)) {
      formIsValid = false;
      errors["budget"] = "Hanya Boleh Angka !";
    } else if (!fields["budget"]) {
      formIsValid = false;
      errors["budget"] = "Jangan Kosong !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  handleValidation_approved = () => {
    let fields = this.state.m_event;
    let errors = {};
    let formIsValid = true;

    if (!fields["assign_to"]) {
      formIsValid = false;
      errors["assign_to"] = "Jangan Kosong !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  onChangePage = (number) => {
    this.setState({
      filter: {
        ...this.state.filter,
        ["page"]: number,
      },
    });
  };

  renderPagination() {
    let items = [];
    const { filter, totaldata } = this.state;
    for (let number = 1; number <= totaldata; number++) {
      items.push(
        <PaginationItem key={number} active={number === filter.page}>
          <PaginationLink onClick={() => this.onChangePage(number)} next>
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return <Pagination>{items}</Pagination>;
  }

  FilterchangeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value,
      },
    });
  };

  loadList = async (filter) => {
    const respon = await eventService.getData(filter);
    if (respon.success) {
      this.setState({
        list_event: respon.result,
      });
    }
  };

  componentDidMount() {
    const { filter } = this.state;
    this.loadList(filter);
  }

  on_cari = () => {
    const { filter } = this.state;
    this.loadList(filter);
  };

  hendlerEdit = async (id) => {
    const respon = await eventService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_modal: true,
        mode: "edit",
        m_event: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
    this.setState({
      errors: {},
    });
  };

  approved = async () => {
    const { m_event, filter } = this.state;
    if (this.handleValidation_approved()) {
      const respon = await eventService.updateData(m_event);
      if (respon.success) {
        alert(respon.result);
      } else {
        alert(respon.result);
      }
      this.loadList(filter);
      this.setState({
        open_approved: false,
      });
    }
  };

  reject = async () => {
    const { m_event, filter } = this.state;
    const respon = await eventService.reject(m_event);
    if (respon.success) {
      alert(respon.result);
    } else {
      alert(respon.result);
    }
    this.loadList(filter);
    this.setState({
      open_approved: false,
      open_reject: false,
    });
  };

  Done = async () => {
    const { m_event, filter } = this.state;
    const respon = await eventService.statusDone(m_event);
    if (respon.success) {
      alert(respon.result);
    } else {
      alert(respon.result);
    }
    this.loadList(filter);
    this.setState({
      open_detail: false,
    });
  };

  onSave = async () => {
    const { m_event, mode, filter } = this.state;

    if (mode === "create") {
      if (this.handleValidation()) {
        const respon = await eventService.post(m_event);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false,
        });
      }
    } else {
      if (this.handleValidation()) {
        const respon = await eventService.updateData(m_event);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.getEmployee();
        this.setState({
          open_modal: false,
          open_approved: true,
        });
      }
    }
  };

  getEmployee = async () => {
    const respon = await eventService.getDataEmployee();
    if (respon.success) {
      this.setState({
        list_employee: respon.result,
      });
    }
  };

  render() {
    const {
      list_employee,
      list_event,
      open_modal,
      mode,
      errors,
      m_event,
      open_approved,
      open_reject,
      open_detail,
    } = this.state;
    return (
      <div>
        &nbsp;<h3>&nbsp;List Event Request</h3>
        <FormInput
          open_modal={open_modal}
          cancel_CE={this.cancel_CE}
          onSave={this.onSave}
          errors={errors}
          list_event={list_event}
          m_event={m_event}
          mode={mode}
          changeHandler={this.changeHandler}
        />
        <Approved
          open_approved={open_approved}
          approved={this.approved}
          m_event={m_event}
          errors={errors}
          cancel_approved={this.cancel_approved}
          select_employee={this.select_employee}
          list_employee={list_employee}
          changeHandler={this.changeHandler}
          openReject={this.openReject}
        />
        <Reject
          close_reject={this.close_reject}
          m_event={m_event}
          reject={this.reject}
          changeHandler={this.changeHandler}
          open_reject={open_reject}
        />
        <Detail
          list_employee={list_employee}
          m_event={m_event}
          closeDetail={this.closeDetail}
          Done={this.Done}
          open_detail={open_detail}
        />
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open_CE}>
            Add
          </button>
        </div>
        <br />
        <br />
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        <div class="form-row">
          <div class="col-md-2.5 mb-3">
            <input
              type="text"
              placeholder="Transaction Code"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Received By"
              id="cariName"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRB")}
            />
          </div>
          <div class="col-md-2 mb-1">
            <input
              type="date"
              id="cariDT"
              class="form-control"
              onChange={this.FilterchangeHandler("searchRD")}
            />
          </div>
          <div class="col-md-1 mb-3">
            <input
              type="text"
              placeholder="Status"
              class="form-control"
              onChange={this.FilterchangeHandler("searchST")}
            />
          </div>
          <div class="col-md-2 mb-1">
            <input
              type="date"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCD")}
            />
          </div>
          <div class="col-md-2.5 mb-3">
            <input
              type="text"
              placeholder="Search Created"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Transaction Code</th>
              <th>Request By</th>
              <th>Request Date</th>
              <th>Status</th>
              <th>Created Date</th>
              <th>Created By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_event.map((data) => {
              return (
                <tr>
                  <td>{data.code}</td>
                  <td>
                    {data.first_name} {data.last_name}
                  </td>
                  <td>{data.tgl}</td>
                  <td>
                    {data.status.toString() === "1"
                      ? "Submited"
                      : data.status.toString() === "2"
                      ? "In Progress"
                      : "Done"}
                  </td>

                  <td>{data.tglcd}</td>
                  <td>{data.created_by}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.id)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      {data.status === 3 ? (
                        <button class="btn btn-primary" disabled>
                          <i class="fa fa-pen"></i>
                        </button>
                      ) : (
                        <button
                          class="btn btn-primary"
                          onClick={() => this.hendlerEdit(data.id)}
                        >
                          <i class="fa fa-pen"></i>
                        </button>
                      )}
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Event;
