import React from "react";
import Modal from "react-bootstrap/Modal";

class Detail extends React.Component {
  render() {
    const { open_detail, m_souvenir, closeDetail, list_unit } = this.props;
    return (
      <Modal show={open_detail}>
        <Modal.Header style={{ background: "lightgrey" }}>
          <Modal.Title>
            View Unit - {m_souvenir.name} ({m_souvenir.code})
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* {JSON.stringify(m_souvenir)} */}
          <from>
          <div class="form-group">
              <label>Souvenir Code *</label>
              <input
                type="text"
                class="form-control"
                value={m_souvenir.code}
                disabled
              />
            </div>
            <div class="form-group">
              <label>Souvenir Name *</label>
              <input
                type="text"
                class="form-control"
                value={m_souvenir.name}
                disabled
              />
            </div>
            <div class="form-group">
              <label>Unit Name *</label>
              <select
                class="form-control"
                disabled
              >
                <option disabled selected>
                  -Choose Unit-
                </option>
                {list_unit.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_souvenir.unitId === data.id}
                    >
                      {data.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div class="form-group">
              <label>Description</label>
              <input
                type="text"
                class="form-control"
                value={
                  m_souvenir.description === "null"
                    ? ""
                    : m_souvenir.description
                }
                disabled
              />
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button className="btn btn-danger" onClick={closeDetail}>
              Close
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Detail;
