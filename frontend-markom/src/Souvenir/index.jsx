import React from "react";
import Table from "react-bootstrap/Table";
import FormInput from "./formInput";
import Detail from "./detail";
import Modal from "react-bootstrap/Modal";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import souvenirService from "../Service/souvenirService";
import unitService from "../Service/unitService";

class Souvenir extends React.Component {
  m_souvenir = {
    name: "",
    description: "",
    mUnitId: "",
  };

  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_delete: false,
      open_detail: false,
      m_souvenir: this.m_souvenir,
      list_souvenir: [],
      list_unit: [],
      mode: "",
      errors: {},
      filter: {
        searchCode: "",
        searchName: "",
        searchCB: "",
        searchDT: "",
        searchUnit: "",
        order: "",
        page: "1",
        pagesize: "10",
      },
      totaldata: 1,
    };
  }

  changeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      m_souvenir: {
        ...this.state.m_souvenir,
        [name]: value,
      },
    });
  };

  open_CE = () => {
    this.getUnit();
    this.setState({
      open_modal: true,
      errors: {},
      m_souvenir: { name: "", description: "", mUnitId: "" },
      mode: "create",
    });
  };

  cancel_CE = () => {
    this.setState({
      open_modal: false,
    });
  };

  closeDetail = () => {
    this.setState({
      open_detail: false,
    });
  };

  closeDelete = () => {
    this.setState({
      open_delete: false,
    });
  };

  handleValidation = () => {
    // const { m_souvenir } = this.state;

    let fields = this.state.m_souvenir;
    let errors = {};
    let formIsValid = true;

    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Jangan Kosong !";
    }

    if (!fields["mUnitId"]) {
      formIsValid = false;
      errors["mUnitId"] = "Jangan Kosong !";
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  loadList = async (filter) => {
    const respon = await souvenirService.getData(filter);
    if (respon.success) {
      this.setState({
        list_souvenir: respon.result,
      });
    }
  };

  hendlerEdit = async (code) => {
    this.getUnit();
    const respon = await souvenirService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_modal: true,
        mode: "edit",
        m_souvenir: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
    this.setState({
      errors: {},
    });
  };

  hendlerDel = async (code) => {
    const respon = await souvenirService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_delete: true,
        m_souvenir: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
  };

  sureDelete = async (item) => {
    const { m_souvenir, filter } = this.state;

    const respon = await souvenirService.deleteSouvenir(m_souvenir);
    if (respon.success) {
      alert(respon.result);
      this.loadList(filter);
    } else {
      alert(respon.result);
    }
    this.setState({
      open_delete: false,
    });
  };

  onSave = async () => {
    const { m_souvenir, mode, filter } = this.state;

    if (mode === "create") {
      if (this.handleValidation()) {
        const respon = await souvenirService.post(m_souvenir);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false,
        });
      }
    } else {
      if (this.handleValidation()) {
        const respon = await souvenirService.updateData(m_souvenir);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false,
        });
      }
    }
  };

  onChangePage = (number) => {
    this.setState({
      filter: {
        ...this.state.filter,
        ["page"]: number,
      },
    });
  };

  renderPagination() {
    let items = [];
    const { filter, totaldata } = this.state;
    for (let number = 1; number <= totaldata; number++) {
      items.push(
        <PaginationItem key={number} active={number === filter.page}>
          <PaginationLink onClick={() => this.onChangePage(number)} next>
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return <Pagination>{items}</Pagination>;
  }

  FilterchangeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value,
      },
    });
  };

  on_cari = () => {
    const { filter } = this.state;
    this.loadList(filter);
  };

  selectHandler_unit = (name) => ({ target: { value } }) => {
    this.setState({
      m_souvenir: {
        ...this.state.m_souvenir,
        [name]: value,
      },
    });
  };

  getUnit = async () => {
    const respon = await unitService.getDataUnit();
    if (respon.success) {
      this.setState({
        list_unit: respon.result,
      });
    }
  };

  componentDidMount() {
    const { filter } = this.state;
    this.loadList(filter);
    this.getUnit();
  }

  openDetail = async (code) => {
    this.getUnit();
    const respon = await souvenirService.getdatabycode(code);
    if (respon.success) {
      this.setState({
        open_detail: true,
        m_souvenir: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
  };

  render() {
    const {
      open_modal,
      list_souvenir,
      list_unit,
      mode,
      errors,
      m_souvenir,
      open_detail,
      open_delete,
    } = this.state;
    return (
      <div>
        &nbsp;<h3>&nbsp;List Souvenir</h3>
        <FormInput
          open_modal={open_modal}
          cancel_CE={this.cancel_CE}
          mode={mode}
          changeHandler={this.changeHandler}
          errors={errors}
          m_souvenir={m_souvenir}
          onSave={this.onSave}
          list_unit={list_unit}
          selectHandler_unit={this.selectHandler_unit}
        />
        <Detail
          open_detail={open_detail}
          m_souvenir={m_souvenir}
          closeDetail={this.closeDetail}
          list_unit={list_unit}
        />

        <Modal show={open_delete} style={{ opacity: 1 }}>
          <Modal.Body>
            Apakah anda akan menghapus data ini {m_souvenir.code} ?
          </Modal.Body>
          <Modal.Footer>
            <div class="btn-group">
              <button
                class="btn btn-success"
                onClick={() => this.sureDelete(m_souvenir.code)}
              >
                Ya
              </button>
              <button class="btn btn-info" onClick={this.closeDelete}>
                Tidak
              </button>
            </div>
          </Modal.Footer>
        </Modal>

        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open_CE}>
            Add
          </button>
        </div>
        <br />
        <br />
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>

        <div class="form-row">
          <div class="col-md-2.6 mb-3">
            <input
              type="text"
              placeholder="Search Code Souvenir"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-2.6 mb-3">
            <input
              type="text"
              placeholder="Search Name Souvenir"
              id="cariName"
              class="form-control"
              onChange={this.FilterchangeHandler("searchName")}
            />
          </div>
          <div class="col-md-2.6 mb-3">
            <select
              class="form-control"
              id="cariUnit"
              onChange={this.FilterchangeHandler("searchUnit")}
            >
              <option disabled selected>
                -Search Unit Name-
              </option>
              {list_unit.map((data) => {
                return <option value={data.name}>{data.name}</option>;
              })}
            </select>
          </div>
          <div class="col-md-2.6 mb-1">
            <input
              type="date"
              id="cariDT"
              class="form-control"
              onChange={this.FilterchangeHandler("searchDT")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Search Created"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Souvenir Code</th>
              <th>Souvenir Name</th>
              <th>Unit</th>
              <th>Created By</th>
              <th>Created Date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_souvenir.map((data) => {
              return (
                <tr>
                  <td>{data.code}</td>
                  <td>{data.name}</td>
                  <td>{data.unitName}</td>
                  <td>{data.created_by}</td>
                  <td>{data.tgl}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.code)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.code)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                      <button
                        class="btn btn-danger"
                        onClick={() => this.hendlerDel(data.code)}
                      >
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <div class="float-right">{this.renderPagination()}</div>
      </div>
    );
  }
}

export default Souvenir;
