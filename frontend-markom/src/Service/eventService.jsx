import axios from "axios";
import { config } from "../Config/config";

export const eventService = {
  getData: (filter) => {
    const result = axios
      .post(config.apiUrl + "/get_t_event", filter)

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    console.log(result);
    return result;
  },

  getdatabyid: (id) => {
    const result = axios
      .get(config.apiUrl + "/get_id_event/" + id)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  post: (item) => {
    const result = axios
      .post(config.apiUrl + "/t_event_post", item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  updateData: (item) => {
    const result = axios
      .put(config.apiUrl + "/t_event_update/" + item.id, item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  reject: (item) => {
    const result = axios
      .put(config.apiUrl + "/t_event_rejected/" + item.id, item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },


  statusDone: (item) => {
    const result = axios
      .put(config.apiUrl + "/t_event_done/" + item.id, item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  getDataEmployee: () => {
    const result = axios
      .get(config.apiUrl + "/get_employee_assignto")

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    console.log(result);
    return result;
  },
};

export default eventService;
