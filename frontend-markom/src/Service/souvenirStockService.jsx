import axios from "axios";
import { config } from "../Config/config";

export const souvenirStockService = {
  post: (item) => {
    const result = axios
      .post(config.apiUrl + "/t_souvenir_post", item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  updateData: (item) => {
    const result = axios
      .put(config.apiUrl + "/t_souvenir_update/" + item.id, item)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  getData: (filter) => {
    const result = axios
      .post(config.apiUrl + "/get_t_souvenir", filter)

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    console.log(result);
    return result;
  },

  getdatabyid: (id) => {
    const result = axios
      .get(config.apiUrl + "/get_id/" + id)
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.result,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  getDataEmployee: () => {
    const result = axios.get(config.apiUrl + '/get_employee' )

        .then(respons => {
            return {
                success: respons.data.success,
                result: respons.data.result
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error
            }
        });
    //console.log(result);
    return result;
},

};

export default souvenirStockService;
