import React from "react";
import Table from "react-bootstrap/Table";
import FormInput from "./formInput";
import Detail from "./detail";
import Modal from "react-bootstrap/Modal";
import employeeService from "../Service/employeeService";

class Employee extends React.Component {
  m_employee = {
    code: "",
    first_name: "",
    last_name: "",
    email: "",
    companyId: "",
  };

  constructor() {
    super();
    this.state = {
      open_modal: false,
      open_detail: false,
      open_delete: false,
      list_employee: [],
      list_company: [],
      mode: "",
      m_employee: this.m_employee,
      errors: {},
      filter: {
        searchCode: "",
        searchName: "",
        searchCN: "",
        searchCB: "",
        searchDT: "",
        order: "",
        page: "1",
        pagesize: "10",
      },
    };
  }

  changeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      m_employee: {
        ...this.state.m_employee,
        [name]: value,
      },
    });
  };

  open = () => {
    this.setState({
      open_modal: true,
      mode: "create",
      m_employee: {
        code: "",
        first_name: "",
        last_name: "",
        email: "",
        companyId: "",
      },
      errors: {},
    });
  };

  close = () => {
    this.setState({
      open_modal: false,
    });
  };

  closeDetail = () => {
    this.setState({
      open_detail: false,
    });
  };

  closeDelete = () => {
    this.setState({
      open_delete: false,
    });
  };

  handleValidation = () => {
    // const { m_employee } = this.state;

    let fields = this.state.m_employee;
    let errors = {};
    let formIsValid = true;

    if (!fields["code"]) {
      formIsValid = false;
      errors["code"] = "Jangan Kosong !";
    } else if (this.state.validasiData > 0) {
      formIsValid = false;
      errors["code"] = "EMP ID Number Sudah ada !";
    } else if (this.state.validasiData_ubah > 0) {
      formIsValid = false;
      errors["code"] = "EMP ID Number Sudah ada !";
    } 

    if (!fields["first_name"]) {
      formIsValid = false;
      errors["first_name"] = "Jangan Kosong !";
    }

    if (!fields["companyId"]) {
      formIsValid = false;
      errors["companyId"] = "Jangan Kosong !";
    }

    //email
    if (fields["email"] === null) {
      formIsValid = true;
    } else if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf("@");
      let lastDotPos = fields["email"].lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          fields["email"].indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          fields["email"].length - lastDotPos > 2
        )
      ) {
        // formIsValid = false;
        // errors["email"] = "email tidak benar";
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  FilterchangeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [name]: value,
      },
    });
  };

  loadList = async (filter) => {
    const respon = await employeeService.getData(filter);
    if (respon.success) {
      this.setState({
        list_employee: respon.result,
      });
    }
  };

  componentDidMount() {
    this.loadList(this.state.filter);
    this.getCompany();
  }

  getCompany = async () => {
    const respon = await employeeService.getDataCompany();
    if (respon.success) {
      this.setState({
        list_company: respon.result,
      });
    }
  };

  selectHandler_company = (name) => ({ target: { value } }) => {
    this.setState({
      m_employee: {
        ...this.state.m_employee,
        [name]: value,
      },
    });
  };

  hendlerEdit = async (id) => {
    const respon = await employeeService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_modal: true,
        mode: "edit",
        m_employee: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
    this.setState({
      errors: {},
    });
  };

  openDetail = async (id) => {
    this.getCompany();
    const respon = await employeeService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_detail: true,
        m_employee: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
  };

  hendlerDel = async (id) => {
    const respon = await employeeService.getdatabyid(id);
    if (respon.success) {
      this.setState({
        open_delete: true,
        m_employee: respon.result[0],
      });
    } else {
      alert(respon.result);
    }
  };

  sureDelete = async (item) => {
    const { m_employee, filter } = this.state;

    const respon = await employeeService.deleteEmployee(m_employee);
    if (respon.success) {
      alert(respon.result);
    } else {
      alert(respon.result);
    }
    this.loadList(filter);
    this.setState({
      open_delete: false,
    });
  };

  onSave = async () => {
    const { m_employee, mode, filter } = this.state;

    if (mode === "create") {
      const respon = await employeeService.validasi(m_employee);
      this.setState({
        validasiData: respon.result.hitung,
      });
      if (this.handleValidation()) {
        const respon = await employeeService.post(m_employee);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false,
        });
      }
    } else {
      const respon = await employeeService.validasiUbah(m_employee);
      this.setState({
        validasiData_ubah: respon.result.hitung,
      });
      if (this.handleValidation()) {
        const respon = await employeeService.updateData(m_employee);
        if (respon.success) {
          alert(respon.result);
        } else {
          alert(respon.result);
        }
        this.loadList(filter);
        this.setState({
          open_modal: false,
        });
      }
    }
  };

  on_cari = () => {
    const { filter } = this.state;
    this.loadList(filter);
  };

  render() {
    const {
      open_detail,
      list_employee,
      list_company,
      open_modal,
      mode,
      errors,
      m_employee,
      open_delete,
    } = this.state;
    return (
      <div>
        &nbsp;<h3>&nbsp;List Employee</h3>
        <FormInput
          open_modal={open_modal}
          close={this.close}
          mode={mode}
          errors={errors}
          m_employee={m_employee}
          list_company={list_company}
          changeHandler={this.changeHandler}
          selectHandler_company={this.selectHandler_company}
          onSave={this.onSave}
        />
        <Detail
          open_detail={open_detail}
          m_employee={m_employee}
          closeDetail={this.closeDetail}
          list_company={list_company}
        />
        <div class="float-right">
          <button type="button" class="btn btn-primary" onClick={this.open}>
            Add
          </button>
        </div>
        <br />
        <br />
        <div class="float-right">
          <button type="button" class="btn btn-warning" onClick={this.on_cari}>
            Search
          </button>
        </div>
        <div class="form-row">
          <div class="col-md-3 mb-3">
            <input
              type="text"
              placeholder="Employee ID Number"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCode")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="Employee Name"
              id="cariCode"
              class="form-control"
              onChange={this.FilterchangeHandler("searchName")}
            />
          </div>
          <div class="col-md-3 mb-3">
            <select
              class="form-control"
              id="cariName"
              onChange={this.FilterchangeHandler("searchCN")}
            >
              <option disabled selected>
                - Select Company Name -
              </option>
              {list_company.map((data) => {
                return <option value={data.name}>{data.name}</option>;
              })}
            </select>
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="date"
              id="cariDT"
              class="form-control"
              onChange={this.FilterchangeHandler("searchDT")}
            />
          </div>
          <div class="col-md-2 mb-3">
            <input
              type="text"
              placeholder="- Search Created -"
              id="cariCB"
              class="form-control"
              onChange={this.FilterchangeHandler("searchCB")}
            />
          </div>
        </div>
        <Modal show={open_delete} style={{ opacity: 1 }}>
          <Modal.Body>
            Apakah anda akan menghapus data ini {m_employee.code} ?
          </Modal.Body>
          <Modal.Footer>
            <div class="btn-group">
              <button
                class="btn btn-success"
                onClick={() => this.sureDelete(m_employee.code)}
              >
                Ya
              </button>
              <button class="btn btn-info" onClick={this.closeDelete}>
                Tidak
              </button>
            </div>
          </Modal.Footer>
        </Modal>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Employee ID Number</th>
              <th>Employee Name</th>
              <th>Company Name</th>
              <th>Create Date</th>
              <th>Created By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {list_employee.map((data) => {
              return (
                <tr>
                  <td>{data.code}</td>
                  <td>
                    {data.first_name} {data.last_name}
                  </td>
                  <td>{data.companyName}</td>
                  <td>{data.tgl}</td>
                  <td>{data.created_by}</td>
                  <td>
                    <div class="btn-group">
                      <button
                        class="btn btn-success"
                        onClick={() => this.openDetail(data.id)}
                      >
                        <i class="fa fa-search"></i>
                      </button>
                      <button
                        class="btn btn-primary"
                        onClick={() => this.hendlerEdit(data.id)}
                      >
                        <i class="fa fa-pen"></i>
                      </button>
                      <button
                        class="btn btn-danger"
                        onClick={() => this.hendlerDel(data.id)}
                      >
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Employee;
