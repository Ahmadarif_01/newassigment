import React from "react";
import Modal from "react-bootstrap/Modal";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      close,
      mode,
      changeHandler,
      errors,
      m_employee,
      list_company,
      onSave,
      selectHandler_company,
    } = this.props;
    return (
      <Modal show={open_modal}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add Employee</label>
            ) : (
              <label>
                Edit Employee - {m_employee.first_name} {m_employee.last_name} (
                {m_employee.code}){" "}
              </label>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {JSON.stringify(m_employee)}
          <from>
            <div class="form-group">
              <label>Emp ID Number *</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Emp ID Number"
                onChange={changeHandler("code")}
                value={m_employee.code}
              />
              <span style={{ color: "red" }}>{errors["code"]}</span>
            </div>
            <div class="form-group">
              <label>First Name *</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type First Name"
                onChange={changeHandler("first_name")}
                value={m_employee.first_name}
              />
              <span style={{ color: "red" }}>{errors["first_name"]}</span>
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input
                type="text"
                class="form-control"
                placeholder="Type Last Name"
                onChange={changeHandler("last_name")}
                value={m_employee.last_name}
              />
            </div>
            <div class="form-group">
              <label>Company Name *</label>
              <select
                class="form-control"
                onChange={selectHandler_company("companyId")}
              >
                <option disabled selected>
                  -Select Company Name-
                </option>
                {list_company.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_employee.companyId === data.id}
                    >
                      {data.name}
                    </option>
                  );
                })}
              </select>
              <span style={{ color: "red" }}>{errors["companyId"]}</span>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Type Email"
                onChange={changeHandler("email")}
                value={m_employee.email === "null" ? "" : m_employee.email}
              />
              <span style={{ color: "red" }}>{errors["email"]}</span>
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div class="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={close}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default formInput;
