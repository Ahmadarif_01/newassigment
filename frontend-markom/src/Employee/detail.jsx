import React from "react";
import Modal from "react-bootstrap/Modal";

class Detail extends React.Component {
  render() {
    const { open_detail, m_employee, closeDetail, list_company } = this.props;
    return (
      <Modal show={open_detail} style={{ opacity: 1 }}>
        <Modal.Header style={{ background: "lightgrey" }}>
          <Modal.Title>
            View Employee - {m_employee.first_name} {m_employee.last_name} (
            {m_employee.code})
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* {JSON.stringify(m_employee)} */}
          <from>
            <div class="form-group">
              <label>Emp ID Number *</label>
              <input
                type="text"
                class="form-control"
                disabled
                value={m_employee.code}
              />
            </div>
            <div class="form-group">
              <label>First Name *</label>
              <input
                type="text"
                class="form-control"
                disabled
                value={m_employee.first_name}
              />
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input
                type="text"
                class="form-control"
                disabled
                value={m_employee.last_name}
              />
            </div>
            <div class="form-group">
              <label>Company Name *</label>
              <select class="form-control" disabled>
                <option disabled selected>
                  -Select Company Name-
                </option>
                {list_company.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_employee.companyId === data.id}
                    >
                      {data.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input
                type="email"
                class="form-control"
                disabled
                value={m_employee.email === "null" ? "" : m_employee.email}
              />
            </div>
          </from>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button className="btn btn-danger" onClick={closeDetail}>
              Close
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Detail;
