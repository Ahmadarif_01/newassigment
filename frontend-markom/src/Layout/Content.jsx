import React from "react";
import Sidebar from "../Layout/Sidebar";
import Header from "../Layout/Header";
import { Route, Switch } from "react-router-dom";
import Souvenir from "../Souvenir";
import Unit from "../Unit";
import Employee from "../Employee";
import souvenirStock from "../SouvenirStock";
import Event from "../Event";

function App() {
  return (
    <div className="wrapper">
      <Header />
      <Sidebar />
      <div className="content-wrapper">
        <section className="content">
          <Switch>
            <Route exact path="/souvenir" component={Souvenir} />
            <Route exact path="/unit" component={Unit} />
            <Route exact path="/employee" component={Employee} />
            <Route exact path="/souvenirstock" component={souvenirStock} />
            <Route exact path="/event" component={Event} />
          </Switch>
        </section>
      </div>
    </div>
  );
}

export default App;
