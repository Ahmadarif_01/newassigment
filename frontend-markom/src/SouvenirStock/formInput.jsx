import React from "react";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/Table";

class formInput extends React.Component {
  render() {
    const {
      open_modal,
      cancel_CE,
      mode,
      changeHandler,
      errors,
      onSave,
      m_souvenir_stock,
      list_employee,
      selectHandler_employee,
      handleAddInput,
      souvenir,
      selectHandler_souvenir,
      changeHandler_souvenir,
      list_souvenir,
      handleDelete
    } = this.props;
    return (
      <Modal show={open_modal} size="md">
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>
            {mode === "create" ? (
              <label>Add Souvenir Stock</label>
            ) : (
              <label>Update Souvenir Stock - {m_souvenir_stock.code}</label>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {JSON.stringify(m_souvenir_stock)}
          <form>
            <div class="form-group">
              <label>Received By *</label>
              <select
                class="form-control"
                onChange={selectHandler_employee("received_by")}
              >
                <option disabled selected>
                  -Select Employee-
                </option>
                {list_employee.map((data) => {
                  return (
                    <option
                      value={data.id}
                      selected={m_souvenir_stock.received_by === data.id}
                    >
                      {data.first_name} {data.last_name}
                    </option>
                  );
                })}
              </select>
              <span style={{ color: "red" }}>{errors["received_by"]}</span>
            </div>
            <div class="form-group">
              <label>Received Date *</label>
              <input
                type="date"
                class="form-control"
                onChange={changeHandler("tgl")}
                value={m_souvenir_stock.tgl}
              />
              <span style={{ color: "red" }}>{errors["tgl"]}</span>
            </div>
            <div class="form-group">
              <label>Note</label>
              <textarea
                type="text"
                class="form-control"
                onChange={changeHandler("note")}
              >
                {m_souvenir_stock.note === "null" ? "" : m_souvenir_stock.note}
              </textarea>
            </div>
          </form>
          <form>
            {/* {JSON.stringify(souvenir)} */}
            <button
              type="button"
              class="btn btn-primary"
              onClick={handleAddInput}
            >
              Add Item
            </button>
            <br />
            <br />
            <Table>
              <thead>
                <tr>
                  <th>Souvenir Name</th>
                  <th>Qty</th>
                  <th>Note</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {m_souvenir_stock.souvenir.map((data, i) => {
                  return (
                    <tr key={i}>
                      <td>
                        <select
                          class="form-control"
                          name="id"
                          onChange={selectHandler_souvenir(
                          m_souvenir_stock.souvenir[i].id = "id"
                          )}
                        >
                          <option selected disabled>
                            -Select Souvenir-
                          </option>
                          {list_souvenir.map((data) => {
                            return (
                              <option
                                value={data.id}
                                // selected={m_souvenir_stock.unitId === data.id}
                              >
                                {data.name}
                              </option>
                            );
                          })}
                        </select>
                      </td>
                      <td>
                        <input
                          class="form-control"
                          type="text"
                          placeholder="Qty"
                          onChange={
                            m_souvenir_stock.souvenir[i].qty = changeHandler_souvenir("qty"
                          )}
                        />
                      </td>
                      <td>
                        <input
                          class="form-control"
                          type="text"
                          placeholder="Note"
                          onChange={
                           changeHandler_souvenir(i.s_note)}
                        />
                      </td>
                      <td><button type="button" class="btn btn-default" onClick={() => handleDelete(m_souvenir_stock.souvenir[i])}><i class="fa fa-trash"></i></button></td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div className="btn-group">
            <button class="btn btn-primary" onClick={onSave}>
              Save
            </button>
            <button class="btn btn-danger" onClick={cancel_CE}>
              Cancel
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default formInput;
